'use strict';

const assert = require( './assert.js' );

const { isVoid, getError, isZMap, getZMapValue } = require( '../../executors/javascript/function-schemata/javascript/src/utils.js' );
const { convertWrappedZObjectToVersionedBinary } = require( '../../executors/javascript/function-schemata/javascript/src/serialize.js' );

function evaluatorIntegrationTest(
	fetch, testRunner, name, input, expectedOutput = null, expectedErrorKeyPhrase = '',
	expectedExtraMetadata = [], expectedMissingMetadata = []
) {
	const wrappedInput = {
		reentrant: false,
		zobject: input
	};
	const wrappedInputV004 = { ...wrappedInput };
	wrappedInputV004.remainingTime = 15;
	const toTest = {};

	toTest[ 'serialized version 0.0.3/4' ] = {
		theInput: convertWrappedZObjectToVersionedBinary( wrappedInputV004, '0.0.4' ),
		contentType: 'application/octet-stream'
	};
	for ( const key of Object.keys( toTest ) ) {
		const theInput = toTest[ key ].theInput;
		const contentType = toTest[ key ].contentType;
		const testName = `${name}: ${key}`;
		testRunner( testName, async function () {
			const fetchedResult = await fetch( {
				method: 'POST',
				body: theInput,
				headers: { 'Content-type': contentType }
			} );
			assert.status( fetchedResult, 200 );
			const jsonRegex = /application\/json/;
			assert.ok( fetchedResult.headers.get( 'Content-type' ).match( jsonRegex ) );
			const Z22 = await fetchedResult.json();
			const Z22K1 = Z22.Z22K1;
			const Z22K2 = Z22.Z22K2;
			if ( expectedOutput !== null ) {
				assert.deepEqual( Z22K1, expectedOutput.Z22K1, name );
				assert.deepEqual( getError( Z22 ), getError( expectedOutput ), name );
				assert.ok( isZMap( Z22K2 ) );
				checkMetadata( name, Z22K2, expectedExtraMetadata, expectedMissingMetadata );
			} else {
				const isError = ( isVoid( Z22K1 ) );
				assert.ok( isError );
				// Checks that the error content contains the expected error key phrase.
				const errorMessage = Z22K2.K1.K1.K2.Z5K2.Z6K1;
				assert.ok( errorMessage.includes( expectedErrorKeyPhrase ) );
			}
		} );
	}
}

function checkMetadata( name, Z22K2, expectedExtraMetadata = [], expectedMissingMetadata = [] ) {
	// Note: Keep this list in sync with calls to setMetadataValue and setMetadataValues in
	// maybeRunZ7(), and in other places (if any).
	// See also: similar code in mswTestRunner.js in the orchestrator.
	const standardMetaData = [
		'evaluationMemoryUsage',
		'evaluationCpuUsage',
		'evaluationStartTime',
		'evaluationEndTime',
		'evaluationDuration',
		'evaluationHostname',
		'executionMemoryUsage',
		'executionCpuUsage'
	];

	standardMetaData.forEach( ( key ) => {
		const normalKey = { Z1K1: 'Z6', Z6K1: key };
		const metaDataValue = getZMapValue( Z22K2, normalKey );
		if ( expectedMissingMetadata.includes( key ) ) {
			assert.deepEqual( metaDataValue, undefined, name + ' should not have the `' + key + '` meta-data key set' );
		} else {
			assert.isDefined( metaDataValue, name + ' should have the `' + key + '` meta-data key set' );
		}
	} );

	expectedExtraMetadata.forEach( ( key ) => {
		const metaDataValue = getZMapValue( Z22K2, key );
		assert.isDefined( metaDataValue, name + ' should have the `' + key + '` meta-data key set' );
	} );
}

module.exports = {
	evaluatorIntegrationTest
};
